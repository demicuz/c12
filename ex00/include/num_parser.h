#ifndef NUM_PARSER_H
# define NUM_PARSER_H

#include "ft_list.h"

typedef struct s_entry
{
	int		power;	// 1
	char	*key;	// "14"
	char	*value;	// "fourteen"
} t_entry;

typedef struct s_dict
{
	t_list *digits;	// 1
	t_list *teens;	// 11
	t_list *tens;   // 90
	t_list *scales;	// malloc(10 * sizeof(t_entry))
	t_list *other;  // 54
} t_dict;

const char	*g_digits[] = {"0", "1", "2", "3", "4", "5", "6", "7", "8", "9", ""};
const char	*g_teens[] = {"10", "11", "12", "13", "14", "15", "16", "17", "18", "19", ""};
const char	*g_tens[] = {"20", "30", "40", "50", "60", "70", "80", "90", ""};

#endif