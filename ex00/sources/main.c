#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <stdio.h>
#include <unistd.h>
#include <stdlib.h>
//TODO wtf bruh
#include "../include/ft_list.h"
#include "../include/num_parser.h"

#define BUF_SIZE 4096

int	ft_strlen(char *str)
{
	int	i;

	i = 0;
	while (*str)
	{
		str++;
		i++;
	}
	return (i);
}

int	ft_strlen_f(char *str, int(*f)(char))
{
	int	i;

	i = 0;
	while (*str && f(*str))
	{
		str++;
		i++;
	}
	return (i);
}

int	ft_isspace(char c)
{
	if (c == '\t' || c == '\n' || c == '\v')
		return (1);
	else if (c == '\f' || c == '\r' || c == ' ')
		return (1);
	return (0);
}

char	*ft_atoi(char *str)
{
	int	result;
	int	sign;

	result = 0;
	sign = 1;
	while (ft_isspace(*str))
		str++;
	if (*str == '-' || *str == '+')
	{
		if (*str == '-')
			return "";
		str++;
	}
	while (*str >= '0' && *str <= '9')
	{
		str++;
	}
	return (void *) 0;
}

int	ft_strcmp_dict(char *s1, char *s2)
{
	while (*s1 && (*s1 == *s2))
	{
		s1++;
		s2++;
	}
	if ((*s1 == ':' || ft_isspace(*s1)) && *s2 == '\0')
		return (0);
	return ((unsigned char) * s1 - (unsigned char) * s2);
}

void	ft_put_from_dict(char *str)
{
	while (*str && *str != ' ')
		str++;
	str++;
	while (*str && *str != '\n')
	{
		write(1, str, 1);
		str++;
	}
}

char *ft_find(char *dict, char *str)
{
	while (*dict)
	{
		if (ft_strcmp_dict(dict, str) == 0)
			return dict;
		else
		{
			while (*dict && *dict != '\n')
				dict++;
			if (*dict)
				dict++;
		}
	}
	return ((void *) 0);
}

char *ft_find_digit(char *dict, char c)
{
	while (*dict)
	{
		if (c == *dict)
			return dict;
		else
		{
			while (*dict && *dict != '\n')
				dict++;
			if (*dict)
				dict++;
			// if (*dict && dict[1] != ':')
			// 	return ((void *) 0);
		}
	}
	return ((void *) 0);
}

char *ft_find_dec(char *dict, char c)
{
	while (*dict)
	{
		if (c == *dict && dict[1] == '0' && dict[2] == ':')
			return dict;
		else
		{
			while (*dict && *dict != '\n')
				dict++;
			if (*dict)
				dict++;
			// if (dict[2] == '0')
			// 	return ((void *) 0);
		}
	}
	return ((void *) 0);
}

void	ft_print_digit(char *dict, char *str)
{
	char	*digit;

	digit = ft_find_digit(dict, *str);
	if (digit)
		ft_put_from_dict(digit);
}

void	ft_print_hundred(char *dict, char *str)
{
	char	*h;
	ft_print_digit(dict, str);

	write(1, " ", 1);

	h = ft_find(dict, "100");
	if (h)
		ft_put_from_dict(h);
}

void	ft_print_dec(char *dict, char *str)
{
	char	*d;

	if (*str == '0')
		return ;
	if (*str == '1')
	{
		d = ft_find(dict, str);
		ft_put_from_dict(d);
		return ;
	}
	d = ft_find_dec(dict, *str);
	if (d)
		ft_put_from_dict(d);
}

void	ft_print_999(char *dict, char *str)
{
	if (ft_strlen(str) == 3)
	{
		ft_print_hundred(dict, str);
		str++;
		if (str[0] != '0' || str[1] != '0')
			write(1, " ", 1);
	}
	if (ft_strlen(str) == 2)
	{
		ft_print_dec(dict, str);
		if (*str == '1')
			return ;
		if (str[0] != '0' && str[1] != '0')
			write(1, " ", 1);
		str++;
	}
	if (ft_strlen(str) == 1)
	{
		if (*str != '0')
			ft_print_digit(dict, str);
	}
}

char	*ft_skip_trash(char *str)
{
	while (ft_isspace(*str))
		str++;
	if (*str == '+')
		str++;
	while (*str == '0')
		str++;
	return str;
}

int	ft_in(const char *a[], char *str, int(*f)(char *, char *))
{
	while (**a)
	{
		if (f(str, (char *) *a) == 0)
			return (1);
		a++;
	}
	return (0);
}

int	ft_add_entry(t_list *list, char *entry)
{
	t_entry	*e;
	char	*str;
	int		size;

	size = ft_strlen_f(entry, &ft_isspace);
	e = malloc(sizeof(t_entry));
	str = malloc(sizeof(char) * (size + 1));

	if (!e || !str)
		return (0);

	e->power = size - 1;
	e->key = str;
	// e->value = val;
	// ft_list_push_front(list, e);
	return (1);
}

int	ft_parse_entry(t_dict *dict, char *entry)
{
	if (!*entry)
		return (1);
	printf("%s\n", entry);
	entry = ft_skip_trash(entry);
	if (ft_in(g_digits, entry, &ft_strcmp_dict))
		if (ft_add_entry(dict->digits, entry))
			return (0);

	return (1);
}

t_dict *ft_init_dict()
{
	t_dict	*dict;

	dict = malloc(sizeof(t_dict));
	if (!dict)
		return (NULL);
	dict->digits = NULL;
	dict->teens = NULL;
	dict->tens = NULL;
	dict->scales = NULL;
	dict->other = NULL;
	return (dict);
}

int	ft_parse_line_by_line(t_dict *dict, int fd, char *buffer)
{
	int	i;
	int	ret;

	i = 0;
	ret = read(fd, buffer, 1);
	while (ret)
	{
		if (buffer[i] == '\n' || buffer[i] == '\0')
		{
			buffer[i] = '\0';
			if (!ft_parse_entry(dict, buffer))
				return (0);
			i = 0;
		}
		else
			i++;
		ret = read(fd, buffer + i, 1);
	}
	return (1);
}

int	ft_parse_all(t_dict *dict, char *file_name)
{
	int		fd;
	char	*buffer;

	buffer = malloc(sizeof(char) * BUF_SIZE);
	fd = open(file_name, O_RDONLY);
	if (fd == -1)
		return (0);
	if (!ft_parse_line_by_line(dict, fd, buffer))
		return (0);
	free(buffer);
	if (close(fd) == -1)
		return (0);
	return (1);
}

int	main(void)
{
	int		parsed;
	t_dict	*dict;

	dict = ft_init_dict();
	if (!dict)
		return (0);
	parsed = ft_parse_all(dict, "numbers.dict");
	if (!parsed)
	{
		puts("Dict Error");
		return (0);
	}

	printf("%s\n", g_digits[5]);
	printf("%d\n", ft_in(g_digits, "99     : yeah", &ft_strcmp_dict));


	return (0);
}
